# -*- coding: utf-8 -*-
# © <2016> <Manexware S.A.>

{
    'name': 'Ctotal - Chart of Accounts',
    'version': '1.1.0',
    'category': 'Localization',
    'author': 'Manexware S.A.',
    'website': 'https://www.manexware.com',
    'license': 'AGPL-3',
    'depends': [
        'account_ec',
    ],
    'data': [
        'data/l10n_ec_chart_data.xml',
        'data/account.account.template.csv',
        'data/l10n_ec_chart_post_data.xml',
        'data/account.tax.code.template.csv',
        'data/account_tax_template_data.xml',
        'data/account_chart_template_data.xml',
    ],
    'installable': True,
}
