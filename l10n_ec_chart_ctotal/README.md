.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

======================================================
Ecuador's Account Chart / Plan de Cuentas para Ecuador
======================================================

This module adds the basic chart of account of Ecuador.
* Works with the specifications of the Superintendencia de Compañías (SUPERCIAS).
* It follows the requirements for SMEs.

Agrega el plan de cuentas para Ecuador.
* Utiliza la clasificación de la Superintendencia de Compañías.
* Sigue los requerimientos para las PYMEs

Tested on version / Probado en la versión
=========================================
10.0-20161103

Installation / Instalación
==========================

To install this module, you need to:
* Delete the standard l10n_ec module from Odoo addons directory.
* Copy this module on the same folder, replacing the deleted one.
* Install this module, no need to install account first as this module will install and configure it properly.

Para instalar este módulo se debe:
* Elimina el módulo l10n_ec de los addons de Odoo.
* Copia este módulo en el mismo directorio, reemplazando el anterior.
* Instalarlo de manera regular, no es necesario instalar account primero, este módulo lo instalará y configurará.

Configuration / Configuración
=============================

This module adds all the accounts as said by the Supercias, some recommendations are:
* You should delete the accounts that don't apply to your company.
* You should add the accounts that are not mandatory but you need.
* As Odoo doesn't use total accounts, you need to use the correct coding to group your accounts correctly, for example, if you need a bank account, and put it inside 10101 account, you should code it as 10101xx.
* The account 10101 has been deleted as it's always better to use detail accounts for bank and cash journals, Odoo will create the accounts 000001 and 000002 for the default bank and cash journals, it's reccommended for you to change them to 1010101 and 1010102 or any other number you like, but you need to change the code.

Este módulo agrega las cuentas contables según la clasificación de la Supercias, sin embargo, te damos algunas recomendaciones:
* Es conveniente eliminar las cuentas contables que no vayan a ser utilizadas por su compañía.
* Debe agregar las cuentas que no son obligatorias, pero desea utilizar.
* Debido a que Odoo no usa cuentas de total, al crear una cuenta, tenga presente la codificación estándar, por ejemplo, si requiere una cuenta de bancos y colocarla dentro de 10101, su código debe ser 10101xx.
* La cuenta