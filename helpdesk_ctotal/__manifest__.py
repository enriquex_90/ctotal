# -*- coding: utf-8 -*-
{
    'name': 'Helpdesk Ctotal',
    'category': 'Customer Relationship Management', 
    'version': '1.0',
    'description': """
Helpdesk Management.
====================

Like records and processing of claims, Helpdesk and Support are good tools
to trace your interventions. This menu is more adapted to oral communication,
which is not necessarily related to a claim. Select a customer, add notes
and categorize your interventions with a channel and a priority level.
    """,
    'author': 'Manexware S.A.',
    'website': 'http://www.manexware.com',
    'depends': ['helpdesk'],
    'data': [
        'data/ticket_sequence.xml',
        'views/helpdesk_ticket_type_views.xml',
        'views/helpdesk_views.xml',
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
