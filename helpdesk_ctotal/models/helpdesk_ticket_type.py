# -*- coding: utf-8 -*-

from odoo import fields, models

class Type(models.Model):

    _inherit = "helpdesk.ticket.type"
    user_ids = fields.Many2many('res.users', string='Usuarios', ondelete='set null',required=True)

