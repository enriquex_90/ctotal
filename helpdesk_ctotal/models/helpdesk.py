# -*- coding: utf-8 -*-

from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import ValidationError

from odoo.tools.translate import _
from datetime import datetime


ATTETION_TIME = [('49', 'More 48'),
                ('48', 'Between 24 and 48'),
                ('24', 'Less 24')]

class HelpdeskTicket(models.Model):

    _inherit = "helpdesk.ticket"

    sequence = fields.Char(String='Ticket Id')
    tikect_id = fields.Char(String='Ticket Id')
    additional_tech_ids = fields.Many2many('res.users', string='Técnicos Adicionales')
    responsable_id = fields.Many2one('res.users', string='Responsable de Ticket', track_visibility='onchange',
                              default=lambda self: self.env.user)
    color = fields.Integer(compute='_compute_color')
    bg_color = fields.Char(compute='_compute_color')
    month = fields.Char(compute='_compute_month_year', store=True,
                        string="Mes")
    year = fields.Char(compute='_compute_month_year', store=True,
                       string=u"Año")
    resolved = fields.Text(string='Resuelto')
    technical_report_id = fields.Char(string=u"Soporte Técnico")
    # technical_report = fields.Char(string="Soporte Técnico")
    short_description = fields.Char(compute='_compute_short_description')
    attention_time = fields.Selection(ATTETION_TIME,
                                      compute='_compute_attention_time',
                                      string=u"Tiempo de Atención", store=True)
    num_parte = fields.Char(string='Num. Parte')
    description = fields.Text(u'descripción', required=False)
    partner_id = fields.Many2one('res.partner', string='Customer', required=True)
    user_id = fields.Many2one(string='Asignar')

    create_manual_date = fields.Datetime('Fecha de creación', default=fields.Datetime.now())
    diagnosis_date = fields.Datetime('Fecha de diagnostico')

    diagnosis_hours = fields.Integer(string='Tiempo de diagnostico (horas)', compute='_compute_diagnosis_hours',
                                  store=True)
    contract_id = fields.Many2one('account.analytic.account', string='Contrato')

    @api.depends('assign_date', 'create_manual_date')
    def _compute_assign_hours(self):
        for ticket in self:
            if ticket.create_manual_date and ticket.assign_date:
                time_difference = fields.Datetime.from_string(ticket.assign_date) - \
                                  fields.Datetime.from_string(ticket.create_manual_date)
                ticket.assign_hours = (time_difference.seconds) / 3600 + time_difference.days * 24

    @api.depends('diagnosis_date', 'create_manual_date')
    def _compute_diagnosis_hours(self):
        for ticket in self:
            if ticket.create_manual_date and ticket.diagnosis_date:
                time_difference = fields.Datetime.from_string(ticket.diagnosis_date) - \
                                  fields.Datetime.from_string(ticket.create_manual_date)
                ticket.diagnosis_hours = (time_difference.seconds) / 3600 + time_difference.days * 24


    @api.depends('close_date', 'create_manual_date')
    def _compute_close_hours(self):
        for ticket in self:
            if ticket.create_manual_date and ticket.close_date:
                time_difference = fields.Datetime.from_string(ticket.close_date) - \
                                  fields.Datetime.from_string(ticket.create_manual_date)
                ticket.close_hours = (time_difference.seconds) / 3600 + time_difference.days * 24


    @api.multi
    def _compute_month_year(self):
        for record in self:
            if record.create_date:
                record.month = fields.Datetime.from_string(
                    record.create_date).month
                record.year = fields.Datetime.from_string(
                    record.create_date).year

    @api.multi
    @api.depends('create_date')
    def _compute_color(self):
        for record in self:
            if record.create_date:
                date_now = fields.Datetime.from_string(fields.Datetime.now())
                create_date = fields.Datetime.from_string(record.create_date)
                total_hours = (date_now - create_date).total_seconds() / 60 / 60
                if total_hours < 24:
                    record.color = 10#green
                    record.bg_color = '#77DD77'  # green
                if total_hours >= 24 and total_hours <= 48:
                    record.color = 3#yellow
                    record.bg_color = '#E9BD15'  # yellow
                if total_hours > 48:
                    record.color = 1#rojo
                    record.bg_color = '#FF6961'  # rojo
            if record.stage_id.is_close:
                record.color = 0  # blanco
                record.bg_color = '#CFCFC4'  # gray

    @api.depends('description')
    def _compute_short_description(self):
        for record in self:
            if record.description:
                record.short_description = record.description[0:40]

    @api.multi
    @api.onchange('ticket_type_id')
    def _onchange_ticket_type_id(self):
        for record in self:
            if record.ticket_type_id:
                user_list = []
                for user in record.ticket_type_id.user_ids:
                    user_list.append(user.id)
                return {'domain': {'user_id': [('id', 'in', user_list)]}}

    @api.multi
    @api.depends('close_date')
    def _compute_attention_time(self):
        for record in self:
            if record.close_date:
                if not record.create_date:
                    raise ValidationError(u"Ingresar fecha de creación")
                else:
                    if record.close_date:
                        date_closed = fields.Datetime.from_string(
                            record.close_date)
                        create_date = fields.Datetime.from_string(
                            record.create_date)
                        total_hours = (
                                                  date_closed - create_date).total_seconds() / 60 / 60
                        if total_hours < 0:
                            raise ValidationError(
                                "Fecha de cierre debe ser mayor a fecha de apertura")
                        if total_hours < 24:
                            record.attention_time = '24'
                        if total_hours >= 24 and total_hours <= 48:
                            record.attention_time = '48'
                        if total_hours > 48:
                            record.attention_time = '49'

    @api.model
    def create(self, vals):
        # set up sequencial
        code = self.env['ir.sequence'].next_by_code('helpdesk.ticket')
        vals['tikect_id'] = "TK-%s" % code
        ticket = super(HelpdeskTicket, self).create(vals)
        if ticket.create_date:
            ticket.month = fields.Datetime.from_string(
                ticket.create_date).month
            ticket.year = fields.Datetime.from_string(ticket.create_date).year
        # "enriquex_90@hotmail.com"
        # 'soporte@conexiontotal.net'
        soporte = self.env['res.partner'].search(
            [('email', '=', "soporte@conexiontotal.net")])
        # notificacion via email
        if soporte:
            ticket.message_subscribe(partner_ids=[soporte.id])
        message = _("<strong>Ticket creado %s</strong><br><br>"
                    "Descripción: %s<br>"
                    "Cliente: %s") % (
                  ticket.tikect_id, ticket.description, ticket.partner_id.name)
        ticket.message_post_with_template("", body=message)
        return ticket

    @api.multi
    def name_get(self):
        result = []
        for ticket in self:
            result.append((ticket.id, "%s (%s)" % (ticket.name, ticket.tikect_id)))
        return result