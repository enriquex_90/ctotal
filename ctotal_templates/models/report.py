# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import AccessError
from odoo.tools import config
from odoo.tools.misc import find_in_path
from odoo.http import request
from odoo.exceptions import UserError

import base64
import logging
import os
import re
import subprocess
import tempfile

from contextlib import closing
from distutils.version import LooseVersion
from reportlab.graphics.barcode import createBarcodeDrawing
from PyPDF2 import PdfFileWriter, PdfFileReader



# A lock occurs when the user wants to print a report having multiple barcode while the server is
# started in threaded-mode. The reason is that reportlab has to build a cache of the T1 fonts
# before rendering a barcode (done in a C extension) and this part is not thread safe. We attempt
# here to init the T1 fonts cache at the start-up of Odoo so that rendering of barcode in multiple
# thread does not lock the server.
try:
    createBarcodeDrawing('Code128', value='foo', format='png', width=100, height=100, humanReadable=1).asString('png')
except Exception:
    pass


#--------------------------------------------------------------------------
# Helpers
#--------------------------------------------------------------------------
_logger = logging.getLogger(__name__)

def _get_wkhtmltopdf_bin():
    return find_in_path('wkhtmltopdf')


#--------------------------------------------------------------------------
# Check the presence of Wkhtmltopdf and return its version at Odoo start-up
#--------------------------------------------------------------------------
wkhtmltopdf_state = 'install'
try:
    process = subprocess.Popen(
        [_get_wkhtmltopdf_bin(), '--version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
except (OSError, IOError):
    _logger.info('You need Wkhtmltopdf to print a pdf version of the reports.')
else:
    _logger.info('Will use the Wkhtmltopdf binary at %s' % _get_wkhtmltopdf_bin())
    out, err = process.communicate()
    match = re.search('([0-9.]+)', out)
    if match:
        version = match.group(0)
        if LooseVersion(version) < LooseVersion('0.12.0'):
            _logger.info('Upgrade Wkhtmltopdf to (at least) 0.12.0')
            wkhtmltopdf_state = 'upgrade'
        else:
            wkhtmltopdf_state = 'ok'

        if config['workers'] == 1:
            _logger.info('You need to start Odoo with at least two workers to print a pdf version of the reports.')
            wkhtmltopdf_state = 'workers'
    else:
        _logger.info('Wkhtmltopdf seems to be broken.')
        wkhtmltopdf_state = 'broken'


class Report(models.Model):
    _inherit = "report"
    _description = "Report"

    public_user = None


    @api.model
    def _run_wkhtmltopdf(self, headers, footers, bodies, landscape, paperformat, spec_paperformat_args=None, save_in_attachment=None, set_viewport_size=False):
        """Execute wkhtmltopdf as a subprocess in order to convert html given in input into a pdf
        document.

        :param header: list of string containing the headers
        :param footer: list of string containing the footers
        :param bodies: list of string containing the reports
        :param landscape: boolean to force the pdf to be rendered under a landscape format
        :param paperformat: ir.actions.report.paperformat to generate the wkhtmltopf arguments
        :param specific_paperformat_args: dict of prioritized paperformat arguments
        :param save_in_attachment: dict of reports to save/load in/from the db
        :returns: Content of the pdf as a string
        """
        header_merge = False
        if not save_in_attachment:
            save_in_attachment = {}

        command_args = []
        if set_viewport_size:
            command_args.extend(['--viewport-size', landscape and '1024x1280' or '1280x1024'])

        # Passing the cookie to wkhtmltopdf in order to resolve internal links.
        try:
            if request:
                command_args.extend(['--cookie', 'session_id', request.session.sid])
        except AttributeError:
            pass

        # Wkhtmltopdf arguments
        command_args.extend(['--quiet'])  # Less verbose error messages
        if paperformat:
            # Convert the paperformat record into arguments
            command_args.extend(self._build_wkhtmltopdf_args(paperformat, spec_paperformat_args))
            if save_in_attachment:
                if bodies:
                    if bodies[0][0]:
                        if hasattr(self.env[save_in_attachment['model']].browse(bodies[0][0]), "h_f"):
                            header_merge = self.env[save_in_attachment['model']].browse(bodies[0][0]).h_f
                        else:
                            header_merge = paperformat[0].header_merge
            else:
                header_merge = paperformat[0].header_merge

        # Force the landscape orientation if necessary
        if landscape and '--orientation' in command_args:
            command_args_copy = list(command_args)
            for index, elem in enumerate(command_args_copy):
                if elem == '--orientation':
                    del command_args[index]
                    del command_args[index]
                    command_args.extend(['--orientation', 'landscape'])
        elif landscape and '--orientation' not in command_args:
            command_args.extend(['--orientation', 'landscape'])

        # Execute WKhtmltopdf
        pdfdocuments = []
        temporary_files = []

        for index, reporthtml in enumerate(bodies):
            local_command_args = []
            pdfreport_fd, pdfreport_path = tempfile.mkstemp(suffix='.pdf', prefix='report.tmp.')
            temporary_files.append(pdfreport_path)

            # Directly load the document if we already have it
            if save_in_attachment and save_in_attachment['loaded_documents'].get(reporthtml[0]):
                with closing(os.fdopen(pdfreport_fd, 'w')) as pdfreport:
                    pdfreport.write(save_in_attachment['loaded_documents'][reporthtml[0]])
                pdfdocuments.append(pdfreport_path)
                continue
            else:
                os.close(pdfreport_fd)

            # Wkhtmltopdf handles header/footer as separate pages. Create them if necessary.
            if not header_merge:
                if headers:
                    head_file_fd, head_file_path = tempfile.mkstemp(suffix='.html', prefix='report.header.tmp.')
                    temporary_files.append(head_file_path)
                    with closing(os.fdopen(head_file_fd, 'w')) as head_file:
                        head_file.write(headers[index])
                    local_command_args.extend(['--header-html', head_file_path])
                if footers:
                    foot_file_fd, foot_file_path = tempfile.mkstemp(suffix='.html', prefix='report.footer.tmp.')
                    temporary_files.append(foot_file_path)
                    with closing(os.fdopen(foot_file_fd, 'w')) as foot_file:
                        foot_file.write(footers[index])
                    local_command_args.extend(['--footer-html', foot_file_path])

            # Body stuff
            content_file_fd, content_file_path = tempfile.mkstemp(suffix='.html', prefix='report.body.tmp.')
            temporary_files.append(content_file_path)
            with closing(os.fdopen(content_file_fd, 'w')) as content_file:
                content_file.write(reporthtml[1])

            try:
                wkhtmltopdf = [_get_wkhtmltopdf_bin()] + command_args + local_command_args
                wkhtmltopdf += [content_file_path] + [pdfreport_path]
                process = subprocess.Popen(wkhtmltopdf, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = process.communicate()

                if process.returncode not in [0, 1]:
                    raise UserError(_('Wkhtmltopdf failed (error code: %s). '
                                      'Message: %s') % (str(process.returncode), err))

                # Save the pdf in attachment if marked
                if reporthtml[0] is not False and save_in_attachment.get(reporthtml[0]):
                    # merge pdf
                    if header_merge:
                        watermar = os.path.join(os.path.dirname(os.path.dirname(__file__)), "pdf/enca_pie.pdf")
                        temp = pdfreport_path
                        new_file = pdfreport_path + "a"
                        temporary_files.append(new_file)

                        template = PdfFileReader(open(temp, 'rb'))

                        output = PdfFileWriter()

                        for i in xrange(template.getNumPages()):
                            wpdf = PdfFileReader(open(watermar, 'rb'))
                            watermark = wpdf.getPage(0)
                            page = watermark
                            page.mergePage(template.getPage(i))
                            output.addPage(page)

                            with open(new_file, 'wb') as f:
                                output.write(f)

                        with open(pdfreport_path + "a", 'rb') as pdfreport:
                            attachment = {
                                'name': save_in_attachment.get(reporthtml[0]),
                                'datas': base64.encodestring(pdfreport.read()),
                                'datas_fname': save_in_attachment.get(reporthtml[0]),
                                'res_model': save_in_attachment.get('model'),
                                'res_id': reporthtml[0],
                            }
                            try:
                                self.env['ir.attachment'].create(attachment)
                            except AccessError:
                                _logger.info("Cannot save PDF report %r as attachment", attachment['name'])
                            else:
                                _logger.info('The PDF document %s is now saved in the database',
                                             attachment['name'])
                    else:
                        with open(pdfreport_path, 'rb') as pdfreport:
                            attachment = {
                                'name': save_in_attachment.get(reporthtml[0]),
                                'datas': base64.encodestring(pdfreport.read()),
                                'datas_fname': save_in_attachment.get(reporthtml[0]),
                                'res_model': save_in_attachment.get('model'),
                                'res_id': reporthtml[0],
                            }
                            try:
                                self.env['ir.attachment'].create(attachment)
                            except AccessError:
                                _logger.info("Cannot save PDF report %r as attachment", attachment['name'])
                            else:
                                _logger.info('The PDF document %s is now saved in the database',
                                             attachment['name'])

                pdfdocuments.append(pdfreport_path)
            except:
                raise

        # Return the entire document
        if len(pdfdocuments) == 1:
            entire_report_path = pdfdocuments[0]
        else:
            entire_report_path = self._merge_pdf(pdfdocuments)
            temporary_files.append(entire_report_path)

        with open(entire_report_path, 'rb') as pdfdocument:
            content = pdfdocument.read()

        #merge pdf
        if header_merge:
            watermar = os.path.join(os.path.dirname(os.path.dirname(__file__)),"pdf/enca_pie.pdf")
            temp = entire_report_path
            new_file = entire_report_path + "a"
            temporary_files.append(new_file)

            template = PdfFileReader(open(temp, 'rb'))
            # wpdf = PdfFileReader(open(watermar, 'rb'))
            # watermark = wpdf.getPage(0)

            output = PdfFileWriter()

            for i in xrange(template.getNumPages()):
                # page = watermark
                wpdf = PdfFileReader(open(watermar, 'rb'))
                watermark = wpdf.getPage(0)
                page = watermark
                page.mergePage(template.getPage(i))
                output.addPage(page)

            # for i in xrange(template.getNumPages()):
            #     page = template.getPage(i)
            #     page.mergePage(watermark)
            #     output.addPage(page)

                with open(new_file, 'wb') as f:
                    output.write(f)

            with open(entire_report_path+"a", 'rb') as pdfdocument:
                content = pdfdocument.read()
        ## en merge
        # Manual cleanup of the temporary files
        for temporary_file in temporary_files:
            try:
                os.unlink(temporary_file)
            except (OSError, IOError):
                _logger.error('Error when trying to remove file %s' % temporary_file)

        return content

