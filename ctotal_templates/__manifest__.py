# -*- coding: utf-8 -*-
# © <2016> <Manexware S.A.>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Ctotal Reportes',
    'version': '0.1.0',
    'author': 'Manexware',
    'category': 'Localization',
    'complexity': 'normal',
    'website': 'http://www.manexware.com',
    'data': [
        'views/report_paperformat_views.xml',
        # 'reports/report_invoice.xml',
        'reports/report_purchaseorder.xml',
        'reports/report_saleorder.xml',
        'reports/report_paperformat.xml',
        'reports/report_supplier_invoice.xml',
        # 'reports/report_external_layout.xml',
        'reports/datetime_template.xml'
    ],
    'depends': [
        'web'
    ],
    'installable': True,
}
