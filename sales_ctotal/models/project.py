from odoo import models, api, fields


class Project(models.Model):
    _description = 'Proyecto'
    _inherit = ['project.project']

    project_sequence = fields.Char('Secuencial', readonly=False)
    project_sequence_id = fields.Char('Secuencial', related='analytic_account_id.analytic_account_sequence', store=True)

    @api.model
    def create(self, vals):
        vals['project_sequence'] = self.env['ir.sequence'].next_by_code('project.sequence')
        preventive = super(Project, self).create(vals)
        return preventive

    # def _create_analytic_account_from_sale_order(self):
    #     for project in self:
    #         analytic_account = self.env['account.analytic.account'].create({
    #             'name': project.name,
    #             'company_id': project.company_id.id,
    #             'partner_id': project.partner_id.id,
    #             'active': True,
    #         })
    #         project.write({'analytic_account_id': analytic_account.id})
