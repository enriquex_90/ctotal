from odoo import models, api, fields


class AnalyticAccount(models.Model):
    _description = 'Cuenta Analítica'
    _inherit = ['account.analytic.account']

    analytic_account_sequence = fields.Char('Secuencial')

    @api.model
    def create(self, vals):
        vals['analytic_account_sequence'] = self.env['ir.sequence'].next_by_code('account.analytic.sequence')
        preventive = super(AnalyticAccount, self).create(vals)
        return preventive
