# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    name_project = fields.Char('Nombre de Proyecto')
    project_id = fields.Many2one('project.project', 'Proyecto', ondelete='restrict')

    @api.multi
    def action_create_project(self):
        if self.project_id:
            raise ValidationError('PROYECTO YA EXISTE')
        else:
            if self.name_project:
                project_project = self.env['project.project']
                result = project_project.sudo().create({
                    'name': self.name_project,
                    'partner_id': self.partner_id.id,
                    'allow_timesheets': True,
                }).id
                self.project_id = result
                # self.project_id._create_analytic_account_from_sale_order()
                self.message_post(message_type='notification', body='Proyecto Creado: ' + self.name_project)
            else:
                raise ValidationError('ESCRIBA NOMBRE DE PROYECTO')
