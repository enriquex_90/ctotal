from odoo import fields, models, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    write_uid = fields.Many2one('res.users', string='Última actualización por',track_visibility="onchange", readonly=True)
    vendor_id = fields.Many2one('res.partner', string='Vendedor', track_visibility="onchange")
