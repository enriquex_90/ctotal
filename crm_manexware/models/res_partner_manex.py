from odoo import models, fields, api


class PartnerManex(models.Model):

    _inherit = 'res.partner'

    @api.model
    def create(self, vals):
        if not vals.get('user_id'):
            vals['user_id'] = self.env.user.id
        return super(PartnerManex, self).create(vals)