# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models

class Stage(models.Model):
    _inherit = "crm.stage"

    new = fields.Boolean('Nuevo')
