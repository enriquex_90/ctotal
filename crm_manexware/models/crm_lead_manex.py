# -*- coding: utf-8 -*-

import logging

from odoo import fields, models,api
from odoo.tools.translate import _
from odoo.exceptions import ValidationError, AccessError

_logger = logging.getLogger(__name__)

class LeadManex(models.Model):
    _inherit = 'crm.lead'

    planned_revenue_recurrent = fields.Float('Expected Revenue Recurrent', track_visibility='always')
    recurrent_months = fields.Integer('Recurrent Months', help="Tiempo del servicio")
    num_quotation = fields.Char(string="No. Quotation", help="Ingrese el numero de cotizacion")
    num_factura = fields.Char(string="No. Factura")
    seguimiento = fields.Many2one('res.users',readonly=1)
    referido = fields.Many2one('res.users', readonly=1)
    soporte_tecnico = fields.Many2one('res.users', readonly=1)
    readonly_state = fields.Boolean(default = True, string='Habilar comisiones')

    @api.model
    def _onchange_stage_id_values(self, stage_id):
        """ returns the new values when stage_id has changed """
        if not stage_id:
            stage_id = self.stage_id.id
        stage = self.env['crm.stage'].browse(stage_id)
        if self.partner_id.parent_id:
            if not stage.new and not self.partner_id.parent_id.ced_ruc:
                raise ValidationError("La matriz debe tener el ruc")
        else:
            if not stage.new and not self.partner_id.ced_ruc:
                raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC/CED")
        vals = super(LeadManex,self)._onchange_stage_id_values(stage_id)

        return vals

    # def compute_readonly_state(self):
    #     for record in self:
    #         if self.env.ref('crm_manexware.group_sale_super_manager').id in self.env.user.groups_id.ids:
    #             record.readonly_state = False
    #         else:
    #             record.readonly_state = True

    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        if self.stage_id.name != 'Nueva' and not self.partner_id.ced_ruc :
            raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC")
        super(LeadManex,self)._onchange_stage_id()

    @api.model
    def create(self, vals):
        context = dict(self._context or {})
        if vals.get('stage_id'):
            vals.update(self._onchange_stage_id_values(vals.get('stage_id')))
        return super(LeadManex, self.with_context(context, mail_create_nolog=True)).create(vals)

    @api.multi
    def write(self, vals):
        # stage change: update date_last_stage_update
        if vals.get('stage_id'):
            stage_name = self.env['crm.stage'].browse(vals.get('stage_id'))
            if stage_name != 'Nueva' and not self.partner_id.ced_ruc:
                raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC")
        if vals.get('partner_id'):
            ced_ruc = self.env['res.partner'].browse(vals.get('partner_id')).ced_ruc
            if self.stage_id.name != 'Nueva' and not ced_ruc:
                raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC")
        if vals.get('partner_id') and vals.get('stage_id'):
            ced_ruc = self.env['res.partner'].browse(vals.get('partner_id')).ced_ruc
            stage_name = self.env['crm.stage'].browse(vals.get('stage_id'))
            if stage_name.name != 'Nueva' and not ced_ruc:
                raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC")
        if self.stage_id.name != 'Nueva' and self.partner_id.ced_ruc:
            if vals.get('partner_id'):
                ced_ruc = self.env['res.partner'].browse(vals.get('partner_id')).ced_ruc
                if not ced_ruc:
                    raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC")
            if 'partner_id' in vals:
                raise ValidationError("No puedes cambiar de estado si el cliente no tiene RUC")
        return super(LeadManex, self).write(vals)

    @api.multi
    def write(self, vals):
        lead = super(LeadManex, self).write(vals)
        if vals.get('stage_id') or vals.get('partner_id'):
            self._onchange_stage_id_values(vals.get('stage_id'))
        return lead