# -*- coding: utf-8 -*-
{
    'name': 'CRM CTOTAL',
    'version': '12.0',
    'category': 'Sales',
    'sequence': 5,
    'summary': 'Leads, Opportunities, Activities',
    'description': """
The generic Odoo Customer Relationship Management
====================================================
Campos Adicionales
""",
    'author': 'Manexware S.A.',
    'website': 'http://www.manexware.com',
    'depends': ['crm','base_ec'
    ],
    'data': [
        # 'data/crm_stage_data.xml',
        'views/crm_lead_manex_views.xml',
        # 'views/crm_stage_views.xml',
        'views/crm_activity_report.xml',
        'security/crm_manex_security.xml',
        'security/ir.model.access.csv'
    ],

    'installable': True,
    'application': False,
    'auto_install': False,
}

