# -*- coding: utf-8 -*-

import logging

from odoo import fields, models,api
from odoo.tools.translate import _
from odoo.exceptions import ValidationError, AccessError

_logger = logging.getLogger(__name__)

class Lead(models.Model):
    _inherit = 'crm.lead'

    tech_inspection_id = fields.Many2one('technical_inspection.ticket',string=u'Inspección Tecnica')
    tech_inspection_date = fields.Datetime(string=u'Fecha')
    tech_inspection_note = fields.Text(string=u'Notas')
    tech_inspection_confirm = fields.Boolean(related='tech_inspection_id.stage_id.confirmed_tech')

    # @api.multi
    # @api.depends('tech_inspection_id.stage_id')
    # def _compute_tech_inspection_confirm(self):
    #     for record in self:
    #         if record.tech_inspection_id.stage_id.confirm_sale:
    #             record.tech_inspection_confirm


    @api.multi
    def create_tech_inspection(self):
        for record in self:
            if record.partner_id and record.tech_inspection_date and record.tech_inspection_note:
                record.alert_state = True
                values = {'name':record.name,
                          'partner_id': record.partner_id.id,
                          'crm_lead_id': record.id,
                          'description':record.tech_inspection_note,
                          'date_proposal':record.tech_inspection_date,
                          'user_id':record.user_id.id}
                tech_inspection_id = self.env['technical_inspection.ticket'].sudo().create(values)
                record.tech_inspection_id = tech_inspection_id
            else:
                raise ValidationError(u"Debes asignar un cliente, fecha propuesta y notas")

    def update_tech_inspection(self):
        for record in self:
            if record.tech_inspection_date and record.tech_inspection_note:
                if record.tech_inspection_id:
                    # tech_inspection = self.env['technical_inspection.ticket'].browse(record.tech_inspection_id.id)
                    record.sudo().tech_inspection_id.date_proposal = record.tech_inspection_date
                    record.sudo().tech_inspection_id.description = record.tech_inspection_note
                    # record.tech_inspection_id.sudo().write({'description' : record.tech_inspection_note})

    def confirm_tech_inspection(self):
        for record in self:
            if record.tech_inspection_date and record.tech_inspection_note:
                if record.tech_inspection_id:
                    tech_inspection_state = self.env['technical_inspection.stage'].search([('confirmed_sales','=',True)])
                    if not tech_inspection_state:
                        raise ValidationError("There isn't state for confirm sale")
                    record.sudo().tech_inspection_id.confirmed_sales = True
                    record.sudo().tech_inspection_id.stage_id = tech_inspection_state[0].id
                    # record.sudo().tech_inspection_id.description = record.tech_inspection_note
                    # record.tech_inspection_id.sudo().write({'description' : record.tech_inspection_note})




