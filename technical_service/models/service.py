# -*- coding: utf-8 -*-

from odoo import models, fields


class Service(models.Model):
    _name = 'technical_service.service'
    _description = "Servicios"
    _rec_name = 'service_name'

    client_id = fields.Many2many('res.partner')
    contract_id = fields.Many2one('account.analytic.account', string='Contrato', domain="[('contract_type', '=', 'sale')]")
    type = fields.Many2one('technical_service.service_type', string='Tipo de Servicio')
    task_ids = fields.Many2many('technical_service.service_tasks', string='Tareas', inverse_name='service_id')
    service_name = fields.Char('Service name')
    product_ids = fields.Many2many('technical_service.product', string='Equipos', inverse_name='service_id')


