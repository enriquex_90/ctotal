# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ProductType(models.Model):
    _name = 'technical_service.product_type'
    _description = "tipo de producto"

    name = fields.Char('Nombre')
    description = fields.Char('Descripción')
