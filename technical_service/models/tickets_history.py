# -*- coding: utf-8 -*-

from odoo import models, fields


class History(models.Model):
    _name = 'technical_service.tickethistory'
    _description = "Historial de ticket"
    _rec_name = 'description'

    radio_id = fields.Many2one('technical_service.product', 'Equipo')
    date = fields.Date(u'Fecha')
    description = fields.Char(u'Descripción')
    client_id = fields.Many2one('res.partner', string='Cliente')