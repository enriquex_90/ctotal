# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
from dateutil.relativedelta import relativedelta

STATE = [('good', 'Bueno'), ('broken', 'Dañado'), ('review', 'Revisión')]


class Product(models.Model):
    _name = 'technical_service.product'
    _description = "Equipos servicio tecnico"
    _rec_name = 'name'

    @api.depends('warranty_date')
    def calculate_date(self):
        for record in self:
            if record.warranty_date:
                today = date.today()
                warranty_date = fields.Date.from_string(record.warranty_date)
                rdelta = relativedelta(warranty_date, today).months
                if rdelta > 3:
                    record.color = '#77DD77'  # green
                elif 1 < rdelta <= 3:
                    record.color = '#E9BD15'  # yellow
                elif rdelta < 1:
                    record.color = '#FF6961'  # rojo

    name = fields.Char(string=u'Código', required=True)
    description = fields.Char('Descripción')
    purchase_date = fields.Date('Fecha de Compra')
    observation = fields.Char(u'Observación')
    state = fields.Selection(STATE, 'Estado')
    ip = fields.Char('IP')
    sale_date = fields.Char('Fecha de Venta')

    service_id = fields.Many2many('technical_service.service', string="Servicios")
    radio_id = fields.Many2many('radio.base')
    ct_property = fields.Boolean(u'¿Propiedad de Conexión Total?')
    license_ids = fields.One2many('technical_service.license', string='Licencias', inverse_name='product_id')
    product_type = fields.Many2one('technical_service.product_type', string='Tipo de Equipo')
    configuration = fields.Text('Configuration')
    ip_ids = fields.One2many('technical_service.ip', string='IPs', inverse_name='product_id')
    user_ids = fields.One2many('technical_service.users', string='Usuarios', inverse_name='service_id')
    warranty_date = fields.Date('Fecha de Garantía')
    warranty_description = fields.Char('Descripción de la garantía')
    mac = fields.Char('MAC')
    serial = fields.Char('Número de Serie')
    brand = fields.Char('Marca')
    model = fields.Char('Modelo')
    history_ids = fields.One2many('technical_service.history', string='Historia', inverse_name='product_id')
    notes = fields.Text('Notas')
    color = fields.Char(compute='calculate_date')
    ignore = fields.Boolean('Ignorar')
    customer_id = fields.Many2one('res.partner', string='Cliente', ondelete="restrict",
                                  domain=[('customer', '=', True)])
    provider_id = fields.Many2one('res.partner', string='Proveedor', ondelete="restrict",
                                  domain=[('supplier', '=', True)])
    helpdesk_ticket_ids = fields.Many2many('helpdesk.ticket')
    product_stock = fields.Many2one('stock.production.lot', string = 'Producto de Inventario')
