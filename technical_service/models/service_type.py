# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ServiceType(models.Model):
    _name = 'technical_service.service_type'
    _description = "Tipo de servicio"

    name = fields.Char('Nombre')
    description = fields.Char('Descripción')