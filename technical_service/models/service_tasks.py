# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ServiceTasks(models.Model):
    _name = 'technical_service.service_tasks'
    _description = "Tarea de Servicios"

    service_id = fields.Many2many('technical_service.service',string="Servicios")
    name = fields.Char('Nombre')
    description = fields.Char('Descrićión')