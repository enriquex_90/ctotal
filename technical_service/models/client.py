# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Client(models.Model):
    _inherit = 'res.partner'
    _name = 'res.partner'

    partner_latitude = fields.Char(u'Latitud')
    partner_longitude = fields.Char(u'Longitud')
    technical_notes = fields.Text(u'Notas técnicas')
    service_ids = fields.Many2many('technical_service.service', string='Servicios', inverse_name='client_id')
    inspection_ids = fields.One2many('technical_inspection.ticket', inverse_name='partner_id', string='Tickets de Inspección',
                                     copy=False)
    inspection_count = fields.Integer("# Inspecciones", compute='_compute_inspection_count')


    @api.multi
    def _compute_inspection_count(self):
        for partner in self:
            partner.inspection_count = self.env['technical_inspection.ticket'].search_count(
                [('partner_id', '=', partner.id)])
