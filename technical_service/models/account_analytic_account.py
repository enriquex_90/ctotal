# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools.translate import _
from datetime import date
from dateutil.relativedelta import relativedelta

MOTIVE = [
    ('best_offer', u'Mejor oferta'),
    ('unlike_product', u'Inconformidad producto'),
    ('unlike_service', u'Inconformidad servicio'),
    ('lack_follow_up', u'Falta seguimiento'),
    ('completion', u'Terminación'),
    ('renewal', u'Renovación')
]

STATES = [
    ('green', 'verde'),
    ('yellow', 'amarillo'),
    ('red', 'rojo')
]

TYPE = [('leasing',u'Alquiler'),
        ('hardware',u'Mantenimientos de Equipos'),
        ('goods',u'Venta de Bienes'),
        ('services',u'Venta de Servicios'),
        ('public',u'Públicos')]

SERVICE_TYPE = [('internet',u'Internet'),
                ('data',u'Transmisión de Datos'),
                ('support',u'Soporte Técnico'),
                ('maintenance_contract', 'Contrato de Mantenimiento'),
                ('sale', 'Venta'),
                ('rental', 'Alquiler')]

STATE = [('new','Nuevo'),
         ('red','Rojo'),
         ('yellow','Amarillo'),
         ('green','Verde'),
         ('finished','Terminado')]


class AccountAnalyticAccount(models.Model):
    _name = 'account.analytic.account'
    _inherit = ['account.analytic.account',
                'account.analytic.contract','mail.thread'
                ]

    @api.depends('end_date', 'partner_id.customer_type', 'ignore')
    def calculate_date(self):
        for record in self:
            if record.end_date:
                today = date.today()
                end_date = fields.Date.from_string(record.end_date)
                rdelta = (end_date - today).days
                if record.partner_id.customer_type == "public":
                    if rdelta > 120:
                        record.state = 'green'
                    elif 60 < rdelta <= 120:
                        record.state = 'yellow'
                    elif rdelta < 60:
                        record.state = 'red'
                else:
                    # elif record.customer_id.customer_type == "private":
                    if rdelta > 60:
                        record.state = 'green'
                    elif 30 < rdelta <= 60:
                        record.state = 'yellow'
                    elif rdelta < 30:
                        record.state = 'red'
            if record.ignore:
                record.state = 'finished'
            if not record.ignore and not record.end_date:
                record.state = 'new'

    sequence = fields.Char('Secuencia', track_visibility='onchange', copy=False)
    reason = fields.Selection(MOTIVE, 'Motivo', help="Motivo de terminacion de contrato", track_visibility='onchange')
    description = fields.Char('Descripción', track_visibility='onchange')
    start_date = fields.Date('Inicio de Contrato', default=fields.Date.context_today, track_visibility='onchange')
    end_date = fields.Date('Fin de Contrato', track_visibility='onchange')
    price = fields.Float('Precio', track_visibility='onchange')
    renewable = fields.Boolean('Renovable', track_visibility='onchange')
    color = fields.Char(compute='calculate_date', track_visibility='onchange')
    ignore = fields.Boolean('Ignorar', track_visibility='onchange')
    type = fields.Selection(TYPE, string=u'Tipo', track_visibility='onchange')
    service_type = fields.Selection(SERVICE_TYPE, string=u'Tipo de Servicio', track_visibility='onchange')
    tag_ids = fields.Many2many('technical.service.contract.tag', string='Etiquetas', track_visibility='onchange')
    state = fields.Selection(STATE, string='Estado',
                             default='new', compute='calculate_date', store=True, track_visibility='onchange')

    user_id = fields.Many2one("res.users", string="Vendedor", track_visibility='onchange')
    notification = fields.Boolean('Notificación recibida', help="Marcar esta casilla si desea que el vendedor no reciba mas correos", track_visibility='onchange')


    @api.model
    def create(self, vals):
        if vals.get('contract_type') == 'sale':
            vals['sequence'] = self.env['ir.sequence'].next_by_code('technical_service.contract.sale')
        if vals.get('contract_type') == 'purchase':
            vals['sequence'] = self.env['ir.sequence'].next_by_code('technical_service.contract.purchase')
        contract = super(AccountAnalyticAccount, self).create(vals)
        # if contract.user_id:
        #     contract.message_subscribe_users(user_ids=[contract.user_id.id])
        # return contract
        return contract

    @api.multi
    def send_mail(self):
        for record in self:
            if record.user_id:
                tmpl = self.env.ref('technical_service.email_contract_alert')
                mail_id = tmpl.send_mail(record.id)
                mail = self.env['mail.mail'].browse(mail_id)
                mail.send(raise_exception=True)

    def send_notification(self):
        contract = self.search(
            [('ignore', '=', False), ('notification', '=', False), ('state', 'in', ['red', 'yellow'])])
        contract.calculate_date()
        contract.send_mail()


class Tag(models.Model):
    _name = "technical.service.contract.tag"
    _description = u"Categoría de Contrato"

    name = fields.Char('Nombre', required=True, translate=True)
    color = fields.Integer('Color Index')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Nombre de etiqueta ya existe !"),
    ]
