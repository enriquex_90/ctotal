# -*- coding: utf-8 -*-

from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import ValidationError

from odoo.tools.translate import _
from datetime import datetime


class HelpdeskTicket(models.Model):

    _inherit = "helpdesk.ticket"
    _name = "helpdesk.ticket"

    technical_service_product_ids = fields.Many2many('technical_service.product', string='Equipos')

