# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CPV(models.Model):
    _name = 'cpv.table'
    _description = "cpv"

    radio_id = fields.Many2many('radio.base')
    cliente_id = fields.Many2one('res.partner', string='Cliente')
    vlan_port = fields.Char('Puerto VLAN')
    radio_id = fields.Many2one('radio.base', string='Radio Base Destino')
    service_id = fields.Many2one('technical_service.service', string='Servicio')
