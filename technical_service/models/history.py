# -*- coding: utf-8 -*-

from odoo import models, fields, api

EQUIPMENT = [
    ('ok', u'Ok'),
    ('to_check', u'Revisar'),
    ('broken', u'Dañado')
]


class History(models.Model):
    _name = 'technical_service.history'
    _description = "Historial servicio técnico"
    _rec_name = 'description'

    product_id = fields.Many2one('technical_service.product', string='Equipo')
    installation_date = fields.Date('Fecha de Instalación')
    place = fields.Char('Lugar')
    description = fields.Char('Descripción')
    equipment_status = fields.Selection(EQUIPMENT,'Estado del Equipo', store=True)
    destiny_id = fields.Many2one('res.partner', string='Destino')
    origin_id = fields.Many2one('res.partner', string='Origen')