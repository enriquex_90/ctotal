# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
from dateutil.relativedelta import relativedelta
import re
from odoo.exceptions import ValidationError
MOTIVE = [
    ('best_offer', u'Mejor oferta'),
    ('unlike_product', u'Inconformidad producto'),
    ('unlike_service', u'Inconformidad servicio'),
    ('lack_follow_up', u'Falta seguimiento'),
    ('completion', u'Terminación'),
    ('renewal', u'Renovación')
]

DURATION = [(1,u'1 Año'),
            (2,u'2 Año'),
            (3,u'3 Año'),
            (4,u'4 Año'),
            (5,u'5 Año')]

STATE = [('new','Nuevo'),
         ('red','Rojo'),
         ('yellow','Amarillo'),
         ('green','Verde'),
         ('finished','Terminado')]


class Licenses(models.Model):
    _name = 'technical_service.license'
    _description = 'Licencia'
    _inherit = ['mail.thread']

    @api.depends('expiration_date','customer_id.customer_type','ignore')
    def calculate_date(self):
        for record in self:
            if record.expiration_date:
                today = date.today()
                expiration_date = fields.Date.from_string(record.expiration_date)
                rdelta = (expiration_date - today).days
                if record.customer_id.customer_type == "public":
                    if rdelta > 120:
                        record.state = 'green'
                        record.color = '#77DD77'  # green
                    elif 60 < rdelta <= 120:
                        record.state = 'yellow'
                        record.color = '#E9BD15'  # yellow
                    elif rdelta < 60:
                        record.state = 'red'
                        record.color = '#FF6961'  # rojo
                else:
                    # elif record.customer_id.customer_type == "private":
                    if rdelta > 60:
                        record.state = 'green'
                        record.color = '#77DD77'  # green
                    elif 30 < rdelta <= 60:
                        record.state = 'yellow'
                        record.color = '#E9BD15'  # yellow
                    elif rdelta < 30:
                        record.state = 'red'
                        record.color = '#FF6961'  # rojo
            if record.ignore:
                record.state ='finished'
            if not record.ignore and not record.expiration_date:
                record.state ='new'


    color = fields.Char(compute='calculate_date')
    ignore = fields.Boolean('Ignorar')

    # registro licencia
    sequence = fields.Char('Secuencia', track_visibility='onchange', copy=False)
    name = fields.Char(string='Nombre de Licencia', required=True, track_visibility='onchange')
    admission_date = fields.Date(u'lic', track_visibility='onchange')
    description = fields.Char(u'Descripción', track_visibility='onchange')
    provider_id = fields.Many2one('res.partner', string='Proveedor', ondelete="restrict", domain=[('supplier','=',True)], track_visibility='onchange')
    duration = fields.Selection(DURATION, string=u'Duración', track_visibility='onchange')

    # proceso de activacion
    activation_date = fields.Date(u'Fecha Activación', track_visibility='onchange')
    expiration_date = fields.Date(u'Fecha Expiración', track_visibility='onchange')
    technical_id = fields.Many2one('res.users',string=u'Técnico', track_visibility='onchange')
    customer_id = fields.Many2one('res.partner', string='Cliente', ondelete="restrict", domain=[('customer','=',True)], track_visibility='onchange')
    product_id = fields.Many2one('technical_service.product', string='Equipo', track_visibility='onchange')
    serial = fields.Char('Serial', size=30, track_visibility='onchange')
    license_key = fields.Char("License Key Generado", track_visibility='onchange')
    email = fields.Char("Correo", track_visibility='onchange')
    location = fields.Char(u"Ubicación", track_visibility='onchange')
    # registro de compra
    purchase_date = fields.Date('Fecha de Compra', track_visibility='onchange')
    invoice_number = fields.Char(u'Número de Factura', track_visibility='onchange')
    nationalitation_number =  fields.Char(u'Número de Nacionalización', track_visibility='onchange')
    responsable_id = fields.Many2one('res.users',string=u'Responsable', track_visibility='onchange')
    number = fields.Integer(u"Número de Licencias", track_visibility='onchange')
    user_id = fields.Many2one("res.users",string="Vendedor", track_visibility='onchange')
    reason = fields.Selection(MOTIVE, 'Motivo', track_visibility='onchange')
    state = fields.Selection(STATE, string='Estado',
                             default='new', compute='calculate_date', store=True)


    notification = fields.Boolean('Notificación recibida', help="Marcar esta casilla si desea que el vendedor no reciba mas correos", track_visibility='onchange')




    @api.constrains('email')
    def validate_mail(self):
        if self.email:
            match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
            if match == None:
                raise ValidationError('Email de cliente invalido')

    @api.model
    def create(self, vals):
        vals['sequence'] = self.env['ir.sequence'].next_by_code('technical_service.license')
        license = super(Licenses, self).create(vals)
        # if license.user_id:
        #     license.message_subscribe_users(user_ids=[license.user_id.id])
        return license


    # @api.multi
    # def write(self, vals):
    #     license = super(Licenses, self).write(vals)
    #     if vals.get('expiration_date', False):
    #         if self.expiration_date:
    #             today = date.today()
    #             expiration_date = fields.Date.from_string(record.expiration_date)
    #             rdelta = (expiration_date - today).days
    #             sent = False
    #             if self.customer_id.customer_type == "private":
    #                 if 30 < rdelta <= 60:
    #                     sent = True
    #                 elif rdelta < 30:
    #                     sent = True
    #
    #             if self.customer_id.customer_type == "public":
    #                 if 60 < rdelta <= 120:
    #                     sent = True
    #                 elif rdelta < 60:
    #                     sent = True
    #             if sent:
    #                 tmpl = self.env.ref('technical_service.email_license_alert')
    #                 mail_id = tmpl.send_mail(self.id)
    #                 mail = self.env['mail.mail'].browse(mail_id)
    #                 mail.send(raise_exception=True)
    #                 self.message_post(body="Revisar esta licencia", message_type='notification')
    #     if vals.get('user_id'):
    #             self.message_subscribe_users(user_ids=[vals['user_id']])
    #     return license


    @api.multi
    def send_mail(self):
        for record in self:
            if record.user_id:
                tmpl = self.env.ref('technical_service.email_license_alert')
                mail_id = tmpl.send_mail(record.id)
                mail = self.env['mail.mail'].browse(mail_id)
                mail.send(raise_exception=True)


    def send_notification(self):
        licenses = self.search([('ignore','=',False),('notification','=',False),('state','in',['red','yellow'])])
        licenses.calculate_date()
        licenses.send_mail()