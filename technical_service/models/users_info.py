# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Users(models.Model):
    _name = 'technical_service.users'
    _description = "Usuario servcicio técnico"

    service_id = fields.Many2one('technical_service.product', string='Equipo')
    name = fields.Char('Nombre de Usuario')
    description = fields.Char('Descripción')
    password = fields.Char('Contraseña')
