# -*- coding: utf-8 -*-

from odoo import models, fields, api


class IP(models.Model):
    _name = 'technical_service.ip'
    _description = "ip info"

    product_id = fields.Many2one('technical_service.product', string='Equipo')
    name = fields.Char('Interface Name')
    description = fields.Char('Description')
    ip_name = fields.Char('IP')
