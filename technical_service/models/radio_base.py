# -*- coding: utf-8 -*-

from odoo import models, fields, api

TIPO = [
    ('radiobase','Radio Base'),
    ('repetitor','Repetidoras')
]
TOWER = [
    ('auto','Autosoportada'),
    ('lineal','Lineal'),
    ('mono','Monopolo')
]


class RadioBase(models.Model):
    _name = 'radio.base'
    _description = "Radio Base"

    name = fields.Char(u'Nombre')
    latitude = fields.Char(u'Latitud')
    length = fields.Char(u'Longitud')
    type = fields.Selection(TIPO, string='Tipo')
    tower = fields.Selection(TOWER, string='Tipo de torre')
    address = fields.Char(u'Dirección')
    product_ids = fields.Many2many('technical_service.product', string=u'Equipos', inverse_name='radio_id')
    notes = fields.Char('Nota')
    ticket_history = fields.Char(u'Historial')
    cpv_ids = fields.Many2many('cpv.table', string=u'Cliente-Puerto-VLan', inverse_name='radio_id')