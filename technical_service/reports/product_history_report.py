# -*- coding: utf-8 -*-

from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
from xlsxwriter.utility import xl_rowcol_to_cell
from odoo import fields
from odoo.exceptions import ValidationError
from datetime import date
from operator import attrgetter
import calendar


class ProductHistoryReport(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, lines):
        # year = int(data['form']['year'])
        # type = data['form']['type']
        # detail = data['form']['detail']
        # company = data['form']['company_id']
        # employees = self.env['hr.employee'].search([])

        sheet = workbook.add_worksheet()
        format21 = workbook.add_format({'font_size': 12, 'align': 'left'})
        sheet.write(0, 0, u'Cédula (Ejm.:0502366503)', format21)
        sheet.write(0, 1, 'Nombres', format21)
        sheet.write(0, 2, 'Apellidos', format21)
        sheet.write(0, 3, u'Genero (Masculino=M ó Femenino=F)', format21)
        sheet.write(0, 4, u'Ocupación(codigo iess)', format21)
        sheet.write(0, 5, u'Total_ganado (Ejm.:1000.56)', format21)
        sheet.write(0, 6, u'Días laborados (360 días equivalen a un año)', format21)
        sheet.write(0, 7,
                    u'Tipo de Pago(Pago Directo=P,Acreditación en Cuenta=A,Retencion Pago Directo=RP,Retencion Acreditación en Cuenta=RA',
                    format21)
        sheet.write(0, 8, u'Solo si el trabajador posee JORNADA PARCIAL PERMANENTE ponga una X', format21)
        sheet.write(0, 9, u'DETERMINE EN HORAS LA JORNADA PARCIAL PERMANENTE SEMANAL ESTIPULADO EN EL CONTRATO',
                    format21)
        sheet.write(0, 10, u'Solo si su trabajador posee algun tipo de discapacidad ponga una X', format21)
        sheet.write(0, 11, u'Ingrese el valor retenido', format21)
        sheet.write(0, 12, u'SOLO SI SU TRABAJADOR MENSUALIZA EL PAGO DE LA DECIMOTERCERA REMUNERACIÓN PONGA UNA X',
                    format21)



ProductHistoryReport('report.product_history_report.xlsx', 'technical_service.history')
