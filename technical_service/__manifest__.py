# -*- coding: utf-8 -*-
{
    'name': "Servicio Técnico",

    'summary': """
        Módulo de servicio técnico para Conexión Total""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Maneware",
    'website': "http://www.manexware.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['helpdesk','technical_inspection','report_xlsx','contract','stock'],

    # always loaded
    'data': [
        # data
        'data/sequence_data.xml',
        'data/ir_cron_data.xml',
        # SECURITY
        'security/technical_service_security.xml',
        'security/ir.model.access.csv',
        # vistas
        'views/account_analytic_account_views.xml',
        # 'views/contract_views.xml',
        'views/radio_base_views.xml',
        'views/service_type_views.xml',
        'views/service_views.xml',
        'views/license_views.xml',
        'views/product_type_views.xml',
        'views/ip_details_views.xml',
        'views/user_info_views.xml',
        'views/history_views.xml',
        'views/client_views.xml',
        'views/product_views.xml',
        'views/helpdesk_views.xml',
        'wizard/product_history_views.xml',
        # 'reports/report_contract_views.xml',
        'reports/report_license_views.xml',
        # 'reports/report_warranty_views.xml',
        'template/mail_templates.xml',
        # MENU
        'views/menu_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}