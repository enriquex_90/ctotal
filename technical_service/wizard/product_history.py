# -*- coding:utf-8 -*-

from odoo import models, fields, api


class ProductHistory(models.TransientModel):
    _name = "product.history"
    _description = "Historia de productos"

    date = fields.Date(string=u'Fecha',)
    partner_id = fields.Many2one('res.partner','Cliente')
    type_id = fields.Many2one('technical_service.product_type', string='Tipo')

    @api.multi
    def export_xls(self):
        context = self._context
        datas = {'ids': context.get('active_ids', [])}
        datas['model'] = 'hr.contract'
        datas['form'] = self.read()[0]
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]
        filename = 'report'
        if context.get('xls_export'):
            return {'type': 'ir.actions.report.xml',
                    'report_name': 'product_history_report.xlsx',
                    'datas': datas,
                    'name': filename
                    }