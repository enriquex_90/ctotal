# Copyright 2017 Tecnativa - Luis M. Ontalba
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

{
    'name': 'Contratos para Servicio Técnico',
    'version': '12.0.1.0.0',
    'category': 'Uncategorized',
    'author': 'Luis Perez, '
              'Manexware',
    'website': 'https://manexware.com',
    'depends': [
        'contract',
        'technical_service',
    ],
    'data': [
        'security/ir.model.access.csv',
        # 'security/account_analytic_account_security.xml',
        'views/account_analytic_account_view.xml',
        # 'views/account_analytic_contract_view.xml',
    ],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': True,
}
