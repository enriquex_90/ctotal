# Copyright 2004-2010 OpenERP SA
# Copyright 2014 Angel Moya <angel.moya@domatix.com>
# Copyright 2015 Pedro M. Baeza <pedro.baeza@tecnativa.com>
# Copyright 2016-2018 Carlos Dauden <carlos.dauden@tecnativa.com>
# Copyright 2016-2017 LasLabs Inc.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from dateutil.relativedelta import relativedelta

from odoo import api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools.translate import _

MOTIVE = [
    ('best_offer', u'Mejor oferta'),
    ('unlike_product', u'Inconformidad producto'),
    ('unlike_service', u'Inconformidad servicio'),
    ('lack_follow_up', u'Falta seguimiento'),
    ('completion', u'Terminación'),
    ('renewal', u'Renovación')
]

STATES = [
    ('green', 'verde'),
    ('yellow', 'amarillo'),
    ('red', 'rojo')
]
TYPE = [('leasing',u'Alquiler'),
        ('hardware',u'Mantenimientos de Equipos'),
        ('goods',u'Venta de Bienes'),
        ('services',u'Venta de Servicios'),
        ('public',u'Públicos')]

SERVICE_TYPE = [('internet',u'Internet'),
                ('data',u'Transmisión de Datos'),
                ('support',u'Soporte Técnico')]

class AccountAnalyticAccount(models.Model):
    _name = 'account.analytic.account'
    _inherit = ['account.analytic.account',
                'account.analytic.contract',
                ]



    reason = fields.Selection(MOTIVE, 'Motive')