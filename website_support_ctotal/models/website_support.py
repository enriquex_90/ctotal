# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from random import randint
import logging
from odoo.http import request

_logger = logging.getLogger(__name__)


class WebsiteSupportTicket(models.Model):
    _inherit = 'website.support.ticket'
    _name = "website.support.ticket"

    branch_partner_id = fields.Many2one('res.partner', string='Sucursal', domain="[('parent_id', '=', partner_id)]")
    contract_id = fields.Many2one('account.analytic.account', string='Contratos')
    additional_tech_ids = fields.Many2many('res.users', string='Técnicos Adicionales')
    attention_time = fields.Selection([('24', 'Menor a 24'), ('48', 'Entre 24 y 48'), ('49', 'Mayor a 48')],
                                      'Tiempo de atención', compute='_compute_attention_time', store=True)
    num_part = fields.Char('Num. Parte')
    technical_report = fields.Char('Soporte Técnico')
    bg_color = fields.Char('Color de fondo', compute='_compute_color')
    color = fields.Integer(compute='_compute_color')
    create_date = fields.Datetime('Creado en')
    create_manual_date = fields.Datetime('Fecha de creación')
    assign_date = fields.Datetime('Fecha de asignación')
    diagnosis_date = fields.Datetime('Fecha de diagnostico')
    assign_hours = fields.Integer('Tiempo de la primera asignación (Horas)', compute='_compute_assign_hours',
                                  store=True)
    diagnosis_hours = fields.Integer('Tiempo de diagnostico (Horas)', compute='_compute_diagnosis_hours', store=True)
    close_hours = fields.Integer('Tiempo abierto (Horas)', compute='_compute_close_hours', store=True)

    responsable_id = fields.Many2one('res.users', string='Responsable de Ticket', track_visibility='onchange',
                                     default=lambda self: self.env.user)
    user_id = fields.Many2one('res.users', string="Usuario Asignado")

    @api.multi
    @api.depends('close_time')
    def _compute_attention_time(self):
        for record in self:
            if record.close_time:
                if not record.create_manual_date:
                    raise ValidationError(u"Ingresar fecha de creación")
                else:
                    if record.close_time:
                        date_closed = fields.Datetime.from_string(
                            record.close_time)
                        create_manual_date = fields.Datetime.from_string(
                            record.create_manual_date)
                        total_hours = (date_closed - create_manual_date).total_seconds() / 60 / 60
                        if total_hours < 0:
                            raise ValidationError(
                                "Fecha de cierre debe ser mayor a fecha de apertura")
                        if total_hours < 24:
                            record.attention_time = '24'
                        if 24 <= total_hours <= 48:
                            record.attention_time = '48'
                        if total_hours > 48:
                            record.attention_time = '49'

    @api.depends('create_manual_date')
    def _compute_color(self):
        for record in self:
            if record.create_manual_date:
                date_now = fields.Datetime.from_string(fields.Datetime.now())
                created_date = fields.Datetime.from_string(record.create_manual_date)
                total_hours = (date_now - created_date).total_seconds() / 60 / 60
                if total_hours < 24:
                    record.color = 10  # green
                    record.bg_color = '#77DD77'  # green
                if 24 <= total_hours <= 48:
                    record.color = 3  # yellow
                    record.bg_color = '#E9BD15'  # yellow
                if total_hours > 48:
                    record.color = 1  # rojo
                    record.bg_color = '#FF6961'  # rojo
            if record.state_id.name == 'Cerrado por cliente' \
                    or record.state_id.name == 'Cerrado por los operarios':
                record.color = 0  # blanco
                record.bg_color = '#CFCFC4'  # gray

    @api.depends('assign_date', 'create_manual_date')
    def _compute_assign_hours(self):
        for ticket in self:
            if ticket.create_manual_date and ticket.assign_date:
                time_difference = fields.Datetime.from_string(ticket.assign_date) - \
                                  fields.Datetime.from_string(ticket.create_manual_date)
                ticket.assign_hours = time_difference.seconds / 3600 + time_difference.days * 24

    @api.depends('close_time', 'create_manual_date')
    def _compute_close_hours(self):
        for ticket in self:
            if ticket.create_manual_date and ticket.close_time:
                time_difference = fields.Datetime.from_string(ticket.close_time) - \
                                  fields.Datetime.from_string(ticket.create_manual_date)
                ticket.close_hours = time_difference.seconds / 3600 + time_difference.days * 24

    @api.depends('diagnosis_date', 'create_manual_date')
    def _compute_diagnosis_hours(self):
        for ticket in self:
            if ticket.create_manual_date and ticket.diagnosis_date:
                time_difference = fields.Datetime.from_string(ticket.diagnosis_date) - \
                                  fields.Datetime.from_string(ticket.create_manual_date)
                ticket.diagnosis_hours = time_difference.seconds / 3600 + time_difference.days * 24

    @api.model
    def create(self, vals):
        new_id = super(WebsiteSupportTicket, self).create(vals)
        soporte = self.env['res.partner'].search(
            [('email', '=', "soporte@conexiontotal.net")])

        if soporte:
            new_id.message_subscribe(partner_ids=[soporte.id])
        message = _("<strong>Ticket creado %s</strong><br><br>"
                    "Descripción: %s<br>"
                    "Cliente: %s") % (
                      new_id.ticket_number, new_id.description, new_id.partner_id.name)
        new_id.message_post_with_template("", body=message)
        return new_id
